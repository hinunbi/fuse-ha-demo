package com.demo;

import com.hazelcast.config.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;

@Configuration
public class HaConfiguration {
  private static final Logger logger = LoggerFactory.getLogger(Application.class);

  private String hostIpAddress;

  @Value("${grid.name}")
  String gridName;

  @Bean
  Config config() {

    Config config = new Config("hazelcastInstance");
    GroupConfig groupConfig = config.getGroupConfig();
    InterfacesConfig interfaces = config.getNetworkConfig().getInterfaces();
    MulticastConfig multicastConfig = config.getNetworkConfig().getJoin().getMulticastConfig();
    @SuppressWarnings("unused")
    Collection<DiscoveryStrategyConfig> discoveryStrategyConfigs =
        config.getNetworkConfig().getJoin().getDiscoveryConfig().getDiscoveryStrategyConfigs();

    groupConfig.setName(gridName);

    interfaces.setEnabled(true);

    // 네트워크 인터페이스 카드 주소 획득
    try {
      InetAddress localHost = InetAddress.getLocalHost();
      hostIpAddress = localHost.getHostAddress();
    } catch (UnknownHostException e) {
    }

    interfaces.addInterface(hostIpAddress);

    // Multicast
    logger.info("Multicast clustering start...");
    multicastConfig.setEnabled(true);
    config.setProperty("hazelcast.discovery.enabled", "false");
    multicastConfig.setMulticastGroup("224.2.2.3");
    multicastConfig.setMulticastPort(54327);

    return config;
  }
}
